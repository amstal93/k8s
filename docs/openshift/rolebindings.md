```
[cloud_user@ip-10-0-2-202 ~]$ sudo -i
[sudo] password for cloud_user:
[root@ip-10-0-2-202 ~]# for i in a b c d ; do htpasswd -b /var/lib/origin/openshift-passwd $i openshift; done
Adding password for user a
Adding password for user b
Adding password for user c
Adding password for user d
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng
> do
> oadm policy add-role-to-user admin a
> done
role "admin" added: "a"
role "admin" added: "a"
role "admin" added: "a"
role "admin" added: "a"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user admin a -n $i ; done
role "admin" added: "a"
role "admin" added: "a"
role "admin" added: "a"
role "admin" added: "a"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user view d -n $i ; done
role "view" added: "d"
role "view" added: "d"
role "view" added: "d"
role "view" added: "d"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user cluster-status c -n $i ; done
role "cluster-status" added: "c"
role "cluster-status" added: "c"
role "cluster-status" added: "c"
role "cluster-status" added: "c"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user edit c -n $i ; done
role "edit" added: "c"
role "edit" added: "c"
role "edit" added: "c"
role "edit" added: "c"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user admin  c -n $i ; done
role "admin" added: "c"
role "admin" added: "c"
role "admin" added: "c"
role "admin" added: "c"
[root@ip-10-0-2-202 ~]# for i in dev prod flight eng; do oadm policy add-role-to-user edit b -n $i ; done
role "edit" added: "b"
role "edit" added: "b"
role "edit" added: "b"
role "edit" added: "b"
[root@ip-10-0-2-202 ~]# oc describe po
pod                  poddisruptionbudget  policy               policybinding
[root@ip-10-0-2-202 ~]# oc describe po
pod                  poddisruptionbudget  policy               policybinding
[root@ip-10-0-2-202 ~]# oc describe policybinding
Name:					:default
Namespace:				default
Created:				14 months ago
Labels:					<none>
Annotations:				<none>
Last Modified:				2019-03-15 05:54:54 +0000 UTC
Policy:					<none>
RoleBinding[admin]:
					Role:			admin
					Users:			a
					Groups:			<none>
					ServiceAccounts:	<none>
					Subjects:		<none>
RoleBinding[system:deployer]:
					Role:			system:deployer
					Users:			<none>
					Groups:			<none>
					ServiceAccounts:	deployer, deployer
					Subjects:		<none>
RoleBinding[system:image-builder]:
					Role:			system:image-builder
					Users:			<none>
					Groups:			<none>
					ServiceAccounts:	builder, builder
					Subjects:		<none>
RoleBinding[system:image-puller]:
					Role:			system:image-puller
					Users:			<none>
					Groups:			system:serviceaccounts:default
					ServiceAccounts:	<none>
					Subjects:		<none>
[root@ip-10-0-2-202 ~]# oc describe policybinding  -n dev
Name:					:default
Namespace:				dev
Created:				25 minutes ago
Labels:					<none>
Annotations:				<none>
Last Modified:				2019-03-15 06:04:54 +0000 UTC
Policy:					<none>
RoleBinding[admin]:
					Role:			admin
					Users:			pinehead, a, c
					Groups:			<none>
					ServiceAccounts:	<none>
					Subjects:		<none>
RoleBinding[cluster-status]:
					Role:			cluster-status
					Users:			c
					Groups:			<none>
					ServiceAccounts:	<none>
					Subjects:		<none>
RoleBinding[edit]:
					Role:			edit
					Users:			c, b
					Groups:			<none>
					ServiceAccounts:	<none>
					Subjects:		<none>
RoleBinding[system:deployers]:
					Role:			system:deployer
					Users:			<none>
					Groups:			<none>
					ServiceAccounts:	deployer
					Subjects:		<none>
RoleBinding[system:image-builders]:
					Role:			system:image-builder
					Users:			<none>
					Groups:			<none>
					ServiceAccounts:	builder
					Subjects:		<none>
RoleBinding[system:image-pullers]:
					Role:			system:image-puller
					Users:			<none>
					Groups:			system:serviceaccounts:dev
					ServiceAccounts:	<none>
					Subjects:		<none>
RoleBinding[view]:
					Role:			view
					Users:			d
					Groups:			<none>
					ServiceAccounts:	<none>
					Subjects:		<none>
```

```
[cloud_user@ip-10-0-2-203 ~]$ oc login -u c -p openshift https://3.84.83.200:8443 -n dev; oc get rolebinding -n dev; oc get rolebinding -n flight
Login successful.

You have access to the following projects and can switch between them with 'oc project <projectname>':

  * dev
    eng
    flight
    prod

Using project "dev".
NAME                    ROLE                    USERS            GROUPS                       SERVICE ACCOUNTS   SUBJECTS
admin                   /admin                  pinehead, a, c
cluster-status          /cluster-status         c
edit                    /edit                   c
system:deployers        /system:deployer                                                      deployer
system:image-builders   /system:image-builder                                                 builder
system:image-pullers    /system:image-puller                     system:serviceaccounts:dev
view                    /view                   d
NAME                    ROLE                    USERS            GROUPS                          SERVICE ACCOUNTS   SUBJECTS
admin                   /admin                  pinehead, a, c
cluster-status          /cluster-status         c
edit                    /edit                   c
system:deployers        /system:deployer                                                         deployer
system:image-builders   /system:image-builder                                                    builder
system:image-pullers    /system:image-puller                     system:serviceaccounts:flight
view                    /view                   d
```